# Generated by Django 4.2.1 on 2023-06-01 21:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0002_rename_acount_receipt_account"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="category",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="receipts",
                to="receipts.expensecategory",
            ),
        ),
    ]
